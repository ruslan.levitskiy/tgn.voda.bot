package main

import "time"

type News struct {
	Hash      string `gorm:"primary_key;AUTO_INCREMENT:false;type:varchar(32)"`
	Text      string `gorm:"type:text"`
	CreatedAt time.Time
}

func (News) TableName() string {
	return "news"
}
