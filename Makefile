all: configure build deploy

configure:
	go get -u github.com/pressly/sup/cmd/sup

build:
	go build ./

deploy:
	sup -D prod deploy
