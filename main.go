package main

import (
	"crypto/md5"
	"encoding/hex"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/golang-collections/collections/stack"
	"github.com/jinzhu/gorm"
	"github.com/labstack/gommon/log"
	"github.com/spf13/viper"
	"golang.org/x/text/encoding/charmap"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	DB  *gorm.DB
	bot *tgbotapi.BotAPI
	err error
)

func main() {
	readConfig()

	connectAndCheck()
	defer DB.Close()

	migrate()

	bot, err = tgbotapi.NewBotAPI(viper.GetString("bot.token"))
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	exit := make(chan interface{})

	// Обработчик входящих сообщений боту
	go func() {
		defer close(exit)
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates, _ := bot.GetUpdatesChan(u)

		log.Info("Getting tg updates started")

		for update := range updates {
			if update.Message == nil {
				continue
			}

			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
			if update.Message != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
				msg.ReplyToMessageID = update.Message.MessageID
				msg.ParseMode = "markdown"
				if update.Message.IsCommand() {
					switch update.Message.Command() {
					case "help":
						msg.Text = "Никто тебе не поможет кроме тебя самого"
					case "start":
						registerSubscriber(update.Message)
						msg.Text = "Привет. Теперь вы будете получать оповещения от меня при появлении новых сообщений об авариях на сайте водоканала г.Таганрога"
					case "last":
						sendLast(update.Message)
					default:
						msg.Text = "Уйди, старуха, я в печали"
					}
				}
				bot.Send(msg)
			}
		}
	}()

	// Получение новых данных со страницы водоканала
	go func() {
		defer close(exit)

		newsRx := regexp.MustCompile("(?ims)<TR><TD bgcolor='#ffffff' width='535'><FONT FACE='VERDANA' SIZE='2'>(.+?)</FONT><P><FONT SIZE='2' FACE='arial'></FONT></P></TD></TR>")

		for {
			var subs []Subscriber
			DB.Find(&subs)

			if response, err := http.Get("http://www.tgnvoda.ru/avarii.php"); err == nil {
				if response.StatusCode == http.StatusOK {
					if contentW1251, err := ioutil.ReadAll(response.Body); err == nil {
						decoder := charmap.Windows1251.NewDecoder()
						content := make([]byte, len(contentW1251)*3)
						if n, _, err := decoder.Transform(content, contentW1251, false); err == nil {
							content = content[:n]
							matches := newsRx.FindAllSubmatch(content, -1)
							var news stack.Stack
							for _, item := range matches {
								itemHash := GetMD5Hash(string(item[1]))
								var sentNews News
								if DB.Where(News{Hash: itemHash}).First(&sentNews).RecordNotFound() {
									news.Push(item[1])
								} else {
									break
								}
							}

							if news.Len() > 0 {
								max := news.Len()

								for i := 0; i < max; i++ {
									item := news.Pop().([]byte)
									itemHash := GetMD5Hash(string(item))
									news := News{
										Hash:      itemHash,
										Text:      string(item),
										CreatedAt: time.Now(),
									}

									err = DB.Save(news).Error
									if err != nil {
										log.Errorf("Error saving news: %s", err.Error())
									}

									newsText := prepareItem(item)

									sendToAll(subs, newsText)
								}
							}
						} else {
							log.Errorf("Error decoding page: %s", err.Error())
						}
					} else {
						log.Warnf("Error read body: %s", err.Error())
					}
				} else {
					log.Warnf("Error status code: %v", response.StatusCode)
				}
				response.Body.Close()
			} else {
				log.Warnf("Error getting page: %s", err.Error())
			}

			if botNews, err := ioutil.ReadFile("./botnews"); err == nil && strings.Trim(string(botNews), " \n") != "" {
				sendToAll(subs, string(botNews))
				if err := os.Remove("./botnews"); err != nil {
					log.Errorf("Error deleting botnews file: %s", err.Error())
				}
			}

			time.Sleep(time.Minute * 5)
		}
	}()

	<-exit
}

func sendToAll(subs []Subscriber, text string) {
	for _, subscriber := range subs {
		msg := tgbotapi.NewMessage(int64(subscriber.ID), "")
		msg.ParseMode = "html"
		msg.Text = text
		bot.Send(msg)
	}
}

func registerSubscriber(msg *tgbotapi.Message) {
	sub := Subscriber{
		ID:           msg.From.ID,
		FirstName:    msg.From.FirstName,
		LastName:     msg.From.LastName,
		Username:     msg.From.UserName,
		LanguageCode: msg.From.LanguageCode,
	}

	DB.FirstOrCreate(&sub)
}

func sendLast(inMsg *tgbotapi.Message) {
	sub := Subscriber{
		ID: inMsg.From.ID,
	}

	var lastNews News
	DB.Order("created_at DESC").First(&lastNews)
	newsText := prepareItem([]byte(lastNews.Text))

	DB.Where(sub).First(&sub)

	msg := tgbotapi.NewMessage(int64(sub.ID), "")
	msg.ReplyToMessageID = inMsg.MessageID
	msg.ParseMode = "html"
	msg.Text = newsText
	bot.Send(msg)
}

func readConfig() {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error config file: %s \n", err)
	}
}

func connectAndCheck() {
	var err error
	DB, err = gorm.Open("mysql", viper.GetString("mysql.conn"))
	if err != nil {
		log.Fatalf("ошибка подключения к базе данных", err.Error())
	}

	DB.DB().SetMaxIdleConns(10)
	DB.DB().SetMaxOpenConns(50)

	//DB = DB.Debug()
}

func migrate() {
	DB.AutoMigrate(
		&Subscriber{},
		&News{},
	)
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func prepareItem(item []byte) string {
	text := string(item)
	text = strings.Replace(text, "<FONT SIZE=2>", "", -1)
	text = strings.Replace(text, "<FONT SIZE='2'>", "", -1)
	text = strings.Replace(text, "</FONT>", "", -1)
	text = strings.Replace(text, "<B></B>", "", -1)
	text = strings.Replace(text, "B>", "b>", -1)
	text = strings.Replace(text, "<BR>", "\n", -1)
	return text
}
