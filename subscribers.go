package main

import "time"

type Subscriber struct {
	ID           int `gorm:"primary_key;AUTO_INCREMENT:false"`
	FirstName    string
	LastName     string
	Username     string
	LanguageCode string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}
